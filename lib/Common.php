<?php
namespace WHMCS\Module\Addon\SsltrustAdmin;

require_once dirname(__FILE__).'/../../../../init.php';
require_once dirname(__FILE__).'/../../../../includes/functions.php';

use WHMCS\Database\Capsule;

class Common {

	public static $config = [];
    public static $configLoaded = false;
    private static $configs = [
                                'authkey',
                                'accesstoken',
                                'tokenexpiry',
                                'basedomain',
                                'customdomain',
                                'partnerid',
                                'hashing', 
                                'lang'
                            ];
    private static $encrypted_keys = [
                                'authkey',
                                'accesstoken',
                                'partnerid'
                            ];

	public function __construct () {
        
	}

	public function getConfig () {
        try{
            $configs = Capsule::table('mod_ssltrust_configuration')
			->select('key', 'value')
			->get();
            foreach ($configs as $config) {
                if(in_array($config->key, Common::$encrypted_keys)){
                    $decrypt = localAPI('DecryptPassword', array('password2' => $config->value));
                    $config->value = $decrypt['password'];
                }
                switch ($config->key) {
                    case 'basedomain':
                        Common::$config[$config->key] = $config->value ? $config->value : 'https://api.ssltrust.com';
                        break;
                    case 'customdomain':
                        Common::$config[$config->key] = $config->value ? $config->value : 'https://www.certmanager.app';
                        break;
                    case 'hashing':
                        Common::$config[$config->key] = $config->value ? $config->value : 'bcrypt';
                        break;
                    case 'lang':
                        Common::$config[$config->key] = $config->value ? $config->value : 'auto';
                        break;
                    default:
                    Common::$config[$config->key] = $config->value;
                        break;
                }
                
            }
            Common::$configLoaded = true;
            // print_r(Common::$config);die();
            return($to_return);
        }
        catch (\Exception $e) {
            logActivity("SSLTrust Module Log|Get Config, {$e->getMessage()}");
        }
    }

    public function setConfigs ($configs) {
        foreach ($configs as $key => $value) {
            Common::setConfig($key, $value);
        }
    }

    public function setConfig ($key, $value) {
        if(!in_array($key, Common::$configs)){
            die('Invalid Config: ' . $key);
        }
        Common::$config[$key] = $value;
        if(in_array($key, Common::$encrypted_keys)){
            $encrypt = localAPI('EncryptPassword', array('password2' => $value));
            $value = $encrypt['password'];
        }
        try{
            Capsule::table('mod_ssltrust_configuration')
            ->updateOrInsert(['key' => $key], ['value' => $value]);
        }
        catch (\Exception $e) {
            logActivity("SSLTrust Module Log|Set Config, {$e->getMessage()}");
        }
    }

    public function generate_config_url($brand, $id, $expiry = 0)
    {
        if (!Common::$configLoaded) { Common::getConfig(); }
        try {
            if ($expiry != 0) {
                $expiry = time() + $expiry;
            }
            if (Common::$config['hashing'] === 'argon') {
                $hash = password_hash('digicert' . '__configuration__' . $id . '__' . $expiry . '__' . Common::$config['authkey'], PASSWORD_ARGON2I);
                $signature = str_replace('=', '_', base64_encode($hash));
            } else {
                $hash = password_hash('digicert' . '__configuration__' . $id . '__' . $expiry . '__' . Common::$config['authkey'], PASSWORD_BCRYPT);
                $signature = 'b-' . str_replace('=', '_', base64_encode($hash));
            }
        } catch (\Exception $e) {
            die('Unable to Generate Configuration URL. Please contact support.');
        }
        return Common::$config['customdomain'] . '/' . Common::$config['partnerid'] .  '/' . ($brand?: 'digicert') . '/configure/' . $id . '/' . $expiry . '/' . $signature . (Common::$config['lang'] !== 'auto' ? '?lang=' . Common::$config['lang'] : '' );
    }

	public function generate_reissue_url($brand, $id, $expiry = 0)
    {
        if (!Common::$configLoaded) { Common::getConfig(); }
        try {
            if ($expiry != 0) {
                $expiry = time() + $expiry;
            }
            if (Common::$config['hashing'] === 'argon') {
                $hash = password_hash('digicert' . '__reissue__' . $id . '__' . $expiry . '__' . Common::$config['authkey'], PASSWORD_ARGON2I);
                $signature = str_replace('=', '_', base64_encode($hash));
            } else {
                $hash = password_hash('digicert' . '__reissue__' . $id . '__' . $expiry . '__' . Common::$config['authkey'], PASSWORD_BCRYPT);
                $signature = 'b-' . str_replace('=', '_', base64_encode($hash));
            }
        } catch (\Exception $e) {
            die('Unable to Generate Reissue URL. Please contact support.');
        }
        return Common::$config['customdomain'] . '/' . Common::$config['partnerid'] .  '/' . ($brand?: 'digicert') . '/reissue/' . $id . '/' . $expiry . '/' . $signature . (Common::$config['lang'] !== 'auto' ? '?lang=' . Common::$config['lang'] : '' );
    }

	public function generate_collect_url($brand, $id, $expiry = 0)
    {
        if (!Common::$configLoaded) { Common::getConfig(); }
        try {
            if ($expiry != 0) {
                $expiry = time() + $expiry;
            }
            if (Common::$config['hashing'] === 'argon') {
                $hash = password_hash('digicert' . '__collect__' . $id . '__' . $expiry . '__' . Common::$config['authkey'], PASSWORD_ARGON2I);
                $signature = str_replace('=', '_', base64_encode($hash));
            } else {
                $hash = password_hash('digicert' . '__collect__' . $id . '__' . $expiry . '__' . Common::$config['authkey'], PASSWORD_BCRYPT);
                $signature = 'b-' . str_replace('=', '_', base64_encode($hash));
            }
        } catch (\Exception $e) {
            die('Unable to Generate Collection URL. Please contact support.');
        }
        return Common::$config['customdomain'] . '/' . Common::$config['partnerid'] .  '/' . ($brand?: 'digicert') . '/collect/' . $id . '/' . $expiry . '/' . $signature . (Common::$config['lang'] !== 'auto' ? '?lang=' . Common::$config['lang'] : '' );
    }

	public function generate_domains_url($brand, $id, $expiry = 0)
    {
        if (!Common::$configLoaded) { Common::getConfig(); }
        try {
            if ($expiry != 0) {
                $expiry = time() + $expiry;
            }
            if (Common::$config['hashing'] === 'argon') {
                $hash = password_hash('digicert' . '__domains__' . $id . '__' . $expiry . '__' . Common::$config['authkey'], PASSWORD_ARGON2I);
                $signature = str_replace('=', '_', base64_encode($hash));
            } else {
                $hash = password_hash('digicert' . '__domains__' . $id . '__' . $expiry . '__' . Common::$config['authkey'], PASSWORD_BCRYPT);
                $signature = 'b-' . str_replace('=', '_', base64_encode($hash));
            }
        } catch (\Exception $e) {
            die('Unable to Generate Domain Management URL. Please contact support.');
        }
        return Common::$config['customdomain'] . '/' . Common::$config['partnerid'] .  '/' . ($brand?: 'digicert') . '/domains/' . $id . '/' . $expiry . '/' . $signature . (Common::$config['lang'] !== 'auto' ? '?lang=' . Common::$config['lang'] : '' );
    }

	public function generate_organisations_url($brand, $id, $expiry = 0)
    {
        if (!Common::$configLoaded) { Common::getConfig(); }
        try {
            if ($expiry != 0) {
                $expiry = time() + $expiry;
            }
            if (Common::$config['hashing'] === 'argon') {
                $hash = password_hash('digicert' . '__organisations__' . $id . '__' . $expiry . '__' . Common::$config['authkey'], PASSWORD_ARGON2I);
                $signature = str_replace('=', '_', base64_encode($hash));
            } else {
                $hash = password_hash('digicert' . '__organisations__' . $id . '__' . $expiry . '__' . Common::$config['authkey'], PASSWORD_BCRYPT);
                $signature = 'b-' . str_replace('=', '_', base64_encode($hash));
            }
        } catch (\Exception $e) {
            die('Unable to Generate Organisation Management URL. Please contact support.');
        }
        return Common::$config['customdomain'] . '/' . Common::$config['partnerid'] .  '/' . ($brand?: 'digicert') . '/organisations/' . $id . '/' . $expiry . '/' . $signature . (Common::$config['lang'] !== 'auto' ? '?lang=' . Common::$config['lang'] : '' );
    }

	public function generate_validationmanager_url($brand, $id, $expiry = 0)
    {
        if (!Common::$configLoaded) { Common::getConfig(); }
        try {
            if ($expiry != 0) {
                $expiry = time() + $expiry;
            }
            if (Common::$config['hashing'] === 'argon') {
                $hash = password_hash('digicert' . '__manage__' . $id . '__' . $expiry . '__' . Common::$config['authkey'], PASSWORD_ARGON2I);
                $signature = str_replace('=', '_', base64_encode($hash));
            } else {
                $hash = password_hash('digicert' . '__manage__' . $id . '__' . $expiry . '__' . Common::$config['authkey'], PASSWORD_BCRYPT);
                $signature = 'b-' . str_replace('=', '_', base64_encode($hash));
            }
        } catch (\Exception $e) {
            die('Unable to Generate Validation Management URL. Please contact support.');
        }
        return Common::$config['customdomain'] . '/' . Common::$config['partnerid'] .  '/' . ($brand?: 'digicert') . '/validation/' . $id . '/' . $expiry . '/' . $signature . (Common::$config['lang'] !== 'auto' ? '?lang=' . Common::$config['lang'] : '' );
    }
}


?>
