<?php
namespace WHMCS\Module\Addon\SsltrustAdmin;

use WHMCS\Database\Capsule;
use GuzzleHttp\Client;
use WHMCS\Module\Addon\SsltrustAdmin\Common;

class SSLTrustAPI {

	const TIMEOUT = '30';

	public function __construct() {

	}

	public function getToken()
    {
		if (!Common::$configLoaded) { Common::getConfig(); }
		try {
			$timestamp = time();
			$toHash = Common::$config['partnerid'] . '__' . $timestamp . '__' . Common::$config['authkey'];
			if (Common::$config['hashing'] === 'argon') {
				$hash = password_hash($toHash, PASSWORD_ARGON2I);
				$signature = str_replace('=', '_', base64_encode($hash));
			} else {
				$hash = password_hash($toHash, PASSWORD_BCRYPT);
				$signature = 'b-' . str_replace('=', '_', base64_encode($hash));
			}
			// echo $toHash;die();
		} catch (\Exception $e) {
			die('Unable to Generate Token Request. Please contact support.');
		}
		$client = new Client();
        try {
            $response = $client->get(Common::$config['basedomain'] . '/auth/new-token', [
					'query' => $params,
					'timeout' => SSLTrustAPI::TIMEOUT,
					'headers' => [
						'Accept'     => 'application/json',
						'x-ssltrust-signature' => $signature,
						'x-ssltrust-timestamp' => $timestamp,
						'x-ssltrust-partner' => Common::$config['partnerid'],
					]
                ]);    
		} catch (\Exception $e) {
			$response = $e->getResponse();
			if (!$response) {
				return ['status' => 'error', 'message' => 'Error requesting Access Token from Provider API.'];
			}
		}
		try {
			// echo $response->getHeader('x-ssltrust-access-token'); die();
			SSLTrustAPI::_updateToken($response->getHeader('x-ssltrust-access-token'), $response->getHeader('x-ssltrust-token-expiry'));
			if ($response->getStatusCode() != 200) {
				$json = SSLTrustAPI::_jsonResponse($response->getBody());
				return ['status' => 'error', 'message' => $json['message'] ?: 'Provider API returned error on request.', 'code' => $json['code'] ?: 'NULL', 'statuscode' => $response->getStatusCode()];
			}
			return ['status' => 'success', 'response' => ''];
		} catch (\Exception $e) {
			return ['status' => 'error', 'message' => 'Error connecting to Provider API.', 'statuscode' => NULL, 'code' => NULL];
		}
		return true;
    }

	public function getProducts()
    {
        return SSLTrustAPI::_get('products');
    }

	public function getService($service)
    {
        return SSLTrustAPI::_get('services/' . $service);
    }

	public function newOrder($order)
    {
        return SSLTrustAPI::_post('orders', $order);
    }

	public function upgradeOrder($order)
    { 
        return SSLTrustAPI::_post('orders/upgrade', $order);
    }

	/**
	 * 
	 * Private Functions
	 * 
	 */


	private function _updateToken($newToken, $expiry) {
		if ($newToken) {
			Common::setConfigs([
				'accesstoken' 	=> $newToken,
				'tokenexpiry'	=> $expiry
			]);
		}
	}

	private function _get($path, $params = [])
    {
        if (!Common::$configLoaded) { Common::getConfig(); }
		$timeNow = new \DateTime("now", new \DateTimeZone("UTC"));
		$tokenExpiry = strtotime(Common::$config['tokenexpiry']);
		if (!Common::$config['accesstoken'] || (Common::$config['accesstoken'] && $tokenExpiry < $timeNow->getTimestamp())) {
			$newToken = SSLTrustAPI::getToken();
			if ($newToken['status'] !== 'success') {
				return $newToken;
			}
		}
        $request = SSLTrustAPI::_getRequest($path, $params);
		if ($request['code'] === 'INVALID_TOKEN') {
			// new token required
			$newToken = SSLTrustAPI::getToken();
			if ($newToken['status'] !== 'success') {
				return $newToken;
			}
			$request = SSLTrustAPI::_getRequest($path, $params);
		}
		return $request;
    }

	private function _getRequest($path, $params = [])
    {
		$client = new Client();
        try {
            $response = $client->get(Common::$config['basedomain'] . '/' . $path, [
                'query' => $params,
                'timeout' => SSLTrustAPI::TIMEOUT,
                'headers' => [
                    'Accept'     => 'application/json',
                    'x-ssltrust-partner' => Common::$config['partnerid'],
					'x-ssltrust-access-token' => Common::$config['accesstoken'],
                ]
                ]);    
		} catch (\Exception $e) {
			$response = $e->getResponse();
			if (!$response) {
				return ['status' => 'error', 'message' => 'Error connecting to Provider API.', 'statuscode' => NULL, 'code' => NULL];
			}
		}
		try {
			$json = SSLTrustAPI::_jsonResponse($response->getBody());
			if ($response->getStatusCode() != 200) {
				return ['status' => 'error', 'message' => $json['message'] ?: 'Provider API returned error on request.', 'code' => $json['code'] ?: 'NULL', 'statuscode' => $response->getStatusCode()];
			}
			return ['status' => 'success', 'response' => $json];
		} catch (\Exception $e) {
			return ['status' => 'error', 'message' => 'Error connecting to Provider API.', 'statuscode' => NULL, 'code' => NULL];
		}
	}

	private function _post($path, $params = [])
    {
        if (!Common::$configLoaded) { Common::getConfig(); }
		$timeNow = new \DateTime("now", new \DateTimeZone("UTC"));
		$tokenExpiry = strtotime(Common::$config['tokenexpiry']);
		if (!Common::$config['accesstoken'] || (Common::$config['accesstoken'] && $tokenExpiry < $timeNow->getTimestamp())) {
			$newToken = SSLTrustAPI::getToken();
			if ($newToken['status'] !== 'success') {
				return $newToken;
			}
		}
        $request = SSLTrustAPI::_postRequest($path, $params);
		if ($request['code'] === 'INVALID_TOKEN') {
			// new token required
			$newToken = SSLTrustAPI::getToken();
			if ($newToken['status'] !== 'success') {
				return $newToken;
			}
			$request = SSLTrustAPI::_postRequest($path, $params);
		}
		return $request;
    }

	private function _postRequest($path, $params = [])
    {
        $client = new Client();
        try {
            $response = $client->post(Common::$config['basedomain'] . '/' . $path, [
                'json' => $params,
                'timeout' => SSLTrustAPI::TIMEOUT,
                'headers' => [
                    'Accept'     => 'application/json',
                    'x-ssltrust-access-token' => Common::$config['accesstoken'],
					'x-ssltrust-partner' => Common::$config['partnerid'],
                ]
                ]);    
        } catch (\Exception $e) {
			$response = $e->getResponse();
			if (!$response) {
				return ['status' => 'error', 'message' => 'Error connecting to Provider API.', 'statuscode' => NULL, 'code' => NULL];
			}
        }
		try {
			$json = SSLTrustAPI::_jsonResponse($response->getBody());
			if ($response->getStatusCode() != 200) {
				return ['status' => 'error', 'message' => $json['message'] ?: 'Provider API returned error on request.', 'code' => $json['code'] ?: 'NULL', 'statuscode' => $response->getStatusCode()];
			}
			
			return ['status' => 'success', 'response' => $json];
		} catch (\Exception $e) {
			return ['status' => 'error', 'message' => 'Error connecting to Provider API.', 'statuscode' => NULL, 'code' => NULL];
        }
    }

	private function _jsonResponse($body)
    {
		$json = json_decode($body, true);
		if ($json === NULL) {
			return ['status' => 'error', 'message' => 'Invalid Response. Not JSON.', 'statuscode' => 500, 'code' => 'NOT_JSON'];
		} else {
			return $json;
		}
	}
}


?>
