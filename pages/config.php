<?php
use WHMCS\Module\Addon\SsltrustAdmin\Common;

if(!defined("WHMCS")){
    die("This file cannot be accessed directly");
}


if ($_POST['action'] === 'save') {
    $toSave = [
        'partnerid'     => (string)$_POST['partnerid'],
        'authkey'       => (string)$_POST['authkey'],
        'basedomain'    => (string)$_POST['basedomain'],
        'customdomain'  => (string)$_POST['customdomain'],
        'hashing'       => (string)$_POST['hashing'],
        'lang'          => (string)$_POST['lang'],
    ];
    Common::setConfigs($toSave);
}
if (!Common::$configLoaded) { Common::getConfig(); }
?>
<?php if($_POST['action'] === 'save') {
if (!Common::$config['partnerid']) { echo '<span class="label closed">Partner ID Required</span><br>'; }
if (!Common::$config['authkey']) { echo '<span class="label closed">API Auth Key Required</span><br>'; }
} ?>
<form method="post" action="addonmodules.php?module=ssltrust_admin&page=config">
    <input type="hidden" name="action" value="save">
    <table class="form" width="100%" border="0" cellspacing="2" cellpadding="3">
        <tbody>
            <tr>
                <td class="fieldlabel">Partner ID</td>
                <td class="fieldarea"><input type="text" name="partnerid"
                        class="form-control input-inline input-100" value="<?= Common::$config['partnerid'] ?>"> <br>Your Reseller/Partner ID</td>
            </tr>
            <tr>
                <td class="fieldlabel">API Auth Key</td>
                <td class="fieldarea"><input type="password" name="authkey"
                        class="form-control input-inline input-600" value="<?= Common::$config['authkey'] ?>"> <br>Supplied by SSLTrust</td>
            </tr>
            <tr>
                <td class="fieldlabel">API Domain</td>
                <td class="fieldarea"><input type="text" name="basedomain"
                        class="form-control input-inline input-400" value="<?= Common::$config['basedomain'] ?  Common::$config['basedomain'] : 'https://api.ssltrust.com' ?>"> <br>Default: https://api.ssltrust.com</td>
            </tr>
            <tr>
                <td class="fieldlabel">Cert Manager Domain</td>
                <td class="fieldarea"><input type="text" name="customdomain"
                        class="form-control input-inline input-400" value="<?= Common::$config['customdomain'] ? Common::$config['customdomain'] : 'https://www.certmanager.app' ?>"> <br>Default: https://www.certmanager.app<br>Setup by SSLTrust required for custom domain.</td>
            </tr>
            <tr>
                <td class="fieldlabel">Hashing/Encryption Method</td>
                <td class="fieldarea"><select name="hashing" class="form-control select-inline">
                        <option value="bcrypt" <?php if (Common::$config['hashing'] === 'bcrypt' || !Common::$config['customdomain']) { echo 'selected="selected"'; } ?>>bcrypt</option>
                       <!-- <option value="argon" <?php if (Common::$config['hashing'] === 'argon') { echo 'selected="selected"'; } ?>>argon ( recommended, but Argon PHP module required )</option> -->
                    </select></td>
            </tr>
            <tr>
                <td class="fieldlabel">Cert Manager Default Language</td>
                <td class="fieldarea"><select name="lang"
                        class="form-control select-inline">
                        <option value="auto"  <?php if (Common::$config['lang'] === 'auto' || !Common::$config['lang']) { echo 'selected="selected"'; } ?>>Auto</option>
                        <option value="en" <?php if (Common::$config['lang'] === 'en') { echo 'selected="selected"'; } ?>>English</option>
                        <option value="de" <?php if (Common::$config['lang'] === 'de') { echo 'selected="selected"'; } ?>>Deutsch</option>
                        <option value="es" <?php if (Common::$config['lang'] === 'es') { echo 'selected="selected"'; } ?>>Español</option>
                        <option value="fr" <?php if (Common::$config['lang'] === 'fr') { echo 'selected="selected"'; } ?>>Français</option>
                        <option value="it" <?php if (Common::$config['lang'] === 'it') { echo 'selected="selected"'; } ?>>Italiano</option>
                        <option value="nl" <?php if (Common::$config['lang'] === 'nl') { echo 'selected="selected"'; } ?>>Nederlands</option>
                        <option value="pt" <?php if (Common::$config['lang'] === 'pt') { echo 'selected="selected"'; } ?>>Português</option>
                        <option value="zh" <?php if (Common::$config['lang'] === 'zh') { echo 'selected="selected"'; } ?>>中文</option>
                    </select></td>
            </tr>
        </tbody>
    </table>
    <div align="center">
        <input type="submit" name="button" value="Save Changes" class="btn btn-default">
    </div>
</form>