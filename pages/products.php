<?php
use WHMCS\Module\Addon\SsltrustAdmin\Common;
use WHMCS\Module\Addon\SsltrustAdmin\SSLTrustAPI;

if(!defined("WHMCS")){
    die("This file cannot be accessed directly");
}

$products = SSLTrustAPI::getProducts();
if ($products['status'] === 'success' && !empty($products['response'])) {
    usort($products['response'], function($a, $b) {return strcmp($a["name"], $b["name"]);});
	foreach ($products['response'] as $product) {
?>
		
        <hr><h2><?= $product['name'] ?></h2><table width="100%" class="datatable">
        <tbody>
            <tr>
            <th scope="col">Details</th>
            <th scope="col">Prices <?= $product['currency'] ?></th>
            </tr>
            <tr>
            <td>
                <b>Type:</b> <?= $product['type'] ?><br>
                <b>Reseller Prod ID:</b> <?= $product['id'] ?><br>
                <b>Terms Available:</b>
                <?php
                $terms = [];
                $san_avail = false;
                $wsan_avail = false;
                foreach ($product['terms'] as $key => $term) {
                    if ($term) array_push($terms, $key);
                }
                echo implode(", ", $terms) . " Months<br>";
                ?>
                <b>Multi-Domain:</b> <?= ($product['multidomain'] ? "Yes" : "No") ?><br>
                <b>Standard SAN:</b> <?= ($product['san'] ? "Yes" : "No") ?><br>
                <b>Wildcard SAN:</b> <?= ($product['wsan'] ? "Yes" : "No") ?>
            </td>
                <td><table width="100%">
                <tbody>
                    <tr>
                    <th scope="col" width="25%">Term</th>
                    <th scope="col" width="25%">Price</th>
                    <th scope="col" width="25%">SAN Price</th>
                    <th scope="col" width="25%">Wildcard SAN Price</th>
                    </tr>
                    <tr>
                    <td class="text-center">12 Months</td>
                    <td class="text-center"><?= ($product['prices']['12'] ? "$" . $product['prices']['12'] : "-") ?></td>
                    <td class="text-center"><?= ($product['sanPrices']['san']['12'] ? "$" . $product['sanPrices']['san']['12'] : "-") ?></td>
                    <td class="text-center"><?= ($product['sanPrices']['wsan']['12'] ? "$" . $product['sanPrices']['wsan']['12'] : "-") ?></td>
                    </tr>
                    <tr>
                    <td class="text-center">24 Months</td>
                    <td class="text-center"><?= ($product['prices']['24'] ? "$" . $product['prices']['24'] : "-") ?></td>
                    <td class="text-center"><?= ($product['sanPrices']['san']['24'] ? "$" . $product['sanPrices']['san']['24'] : "-") ?></td>
                    <td class="text-center"><?= ($product['sanPrices']['wsan']['24'] ? "$" . $product['sanPrices']['wsan']['24'] : "-") ?></td>
                    </tr>
                    <tr>
                    <td class="text-center">36 Months</td>
                    <td class="text-center"><?= ($product['prices']['36'] ? "$" . $product['prices']['36'] : "-") ?></td>
                    <td class="text-center"><?= ($product['sanPrices']['san']['36'] ? "$" . $product['sanPrices']['san']['36'] : "-") ?></td>
                    <td class="text-center"><?= ($product['sanPrices']['wsan']['36'] ? "$" . $product['sanPrices']['wsan']['36'] : "-") ?></td>
                    </tr>
                    <tr>
                    <td class="text-center">48 Months</td>
                    <td class="text-center"><?= ($product['prices']['48'] ? "$" . $product['prices']['12'] : "-") ?></td>
                    <td class="text-center"><?= ($product['sanPrices']['san']['48'] ? "$" . $product['sanPrices']['san']['48'] : "-") ?></td>
                    <td class="text-center"><?= ($product['sanPrices']['wsan']['48'] ? "$" . $product['sanPrices']['wsan']['48'] : "-") ?></td>
                    </tr>
                    <tr>
                    <td class="text-center">60 Months</td>
                    <td class="text-center"><?= ($product['prices']['60'] ? "$" . $product['prices']['60'] : "-") ?></td>
                    <td class="text-center"><?= ($product['sanPrices']['san']['60'] ? "$" . $product['sanPrices']['san']['60'] : "-") ?></td>
                    <td class="text-center"><?= ($product['sanPrices']['wsan']['60'] ? "$" . $product['sanPrices']['wsan']['60'] : "-") ?></td>
                    </tr>
                    <tr>
                    <td class="text-center">72 Months</td>
                    <td class="text-center"><?= ($product['prices']['72'] ? "$" . $product['prices']['72'] : "-") ?></td>
                    <td class="text-center"><?= ($product['sanPrices']['san']['72'] ? "$" . $product['sanPrices']['san']['72'] : "-") ?></td>
                    <td class="text-center"><?= ($product['sanPrices']['wsan']['72'] ? "$" . $product['sanPrices']['wsan']['72'] : "-") ?></td>
                    </tr>
                </tbody>
                </table></td>
            </tr>
            </tbody>
        </table>
        </body>
<?php
	}
} else {
    echo "No products available, or unable to contact API.";
}
?>