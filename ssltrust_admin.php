<?php
//
// for any support please contact support@ssltrust.com
// this module requires the SSLTrust Provisioning Module to be installed
//

if (!defined("WHMCS")) {
    die("This file cannot be accessed directly");
}

use WHMCS\Module\Addon\SsltrustAdmin\SSLTrustAPI;
use WHMCS\Module\Addon\SsltrustAdmin\Common;
use WHMCS\Database\Capsule;

function ssltrust_admin_config() {
	global $CONFIG;
	$configarray = array(
		"name" => "SSLTrust Admin",
		"version" => "3.0.0",
		"author" => "<a href='https://www.ssltrust.com' target='_blank'>SSLTrust</a>",
		"language" => "english");
  return $configarray;
}

function ssltrust_admin_activate() {
	try {
			if (!Capsule::schema()->hasTable('mod_ssltrust_configuration')) {
			    Capsule::schema()->create(
			        'mod_ssltrust_configuration',
			        function ($table) {
			            $table->increments('id');
						$table->string('key', 100);
						$table->string('value', 255);
						$table->dateTime('created')->nullable()->useCurrent();
						$table->dateTime('modified')->nullable()->default(Capsule::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
			        }
			    );
			}
			if (!Capsule::schema()->hasTable('mod_ssltrust_orders')) {
			    Capsule::schema()->create(
			        'mod_ssltrust_orders',
			        function ($table) {
			            $table->increments('id');
                  		$table->integer('whmcs_service_id');
						$table->integer('whmcs_package_id')->nullable();
			            $table->integer('ssltrust_serviceid');
						$table->integer('san')->default('0');
						$table->integer('wsan')->default('0');
						$table->dateTime('ordered')->nullable()->useCurrent();
						$table->dateTime('modified')->nullable()->default(Capsule::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
			        }
			    );
			}
			if (!Capsule::schema()->hasTable('mod_ssltrust_upgrades')) {
			    Capsule::schema()->create(
			        'mod_ssltrust_upgrades',
			        function ($table) {
			            $table->increments('id');
                  		$table->integer('whmcs_service_id');
						$table->integer('whmcs_package_id')->nullable();
						$table->dateTime('ordered')->nullable()->useCurrent();
						$table->dateTime('modified')->nullable()->default(Capsule::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
			        }
			    );
			}

			return [
				'status' => 'success',
				'description' => 'Successfully Activated SSLTrust Module. Please enter your API Tokens to get started.',
			];
		} catch (\Exception $e) {
        	return [
            	'status' => "error",
            	'description' => 'Unable to create mod_ssltrust_orders table: ' . $e->getMessage(),
        	];
		}
}

function ssltrust_admin_upgrade() {
  	//nothing yet
}

function ssltrust_admin_deactivate() {
	//nothing yet
}

function ssltrust_admin_sidebar($vars)
{
    $sidebar = '<div class="sidebar-header">
					<i class="fas fa-shield"></i>
					SSLTrust Admin
				</div>
				<ul class="menu">
					<li><a href="addonmodules.php?module=ssltrust_admin">Dashboard</a></li>
					<li><a href="addonmodules.php?module=ssltrust_admin&page=config">Configuration</a></li>
					<li><a href="addonmodules.php?module=ssltrust_admin&page=products">Products</a></li>
					<li></li>
					<li><a href="https://www.ssltrust.com.au/manager/clientarea.php" target="_blank">SSLTrust Account <i class="fas fa-arrow-circle-right"></i></a></li>
				</ul>';
    return $sidebar;
}

function ssltrust_admin_output($vars) {
	$page = (isset($_GET['page'])) ? $_GET['page'] : '';
	switch($page)
    {
        case 'config':
            require_once(dirname(__FILE__) . '/pages/config.php');
            break;
		case 'products':
			require_once(dirname(__FILE__) . '/pages/products.php');
			break;
        default:
			if (!Common::$configLoaded) { Common::getConfig(); }
            if(empty(Common::$config['authkey']) || empty(Common::$config['partnerid'])){
                require_once(dirname(__FILE__) . '/pages/config.php');
            }
            else{
                require_once(dirname(__FILE__) . '/pages/dashboard.php');
            }
    }
}

?>
